# README #

This is a quick and dirty demo of using brace expansion to do the following:
	- change the name to lower-case
	- remove spaces and replace it with underscores
	- print the result of whats about to happen
	- commit the switch to new file name

!!! WARNING !!!
This script will run immediatly in the current directory and alter the changes.
Run only if you know what you are doing.
